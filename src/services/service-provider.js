import AdvisorService from './advisor-service';
import TechniqueService from './technique-service';
import ProgramService from './program-service';
import SemesterService from './semester-service';
import TopicService from './topic-service';
import GroupService from './group-service';
import StudentService from './student-service';
import CommitteeService from './committee-service';
import DHService from './department-head-service';
import NotiService from './notification-service';
import UserService from './user-service';
import TrainingStaffService from './trainning-staff-service';
import CommonService from './common-service';

export {
  CommonService,
  AdvisorService,
  TechniqueService,
  ProgramService,
  SemesterService,
  TopicService,
  GroupService,
  StudentService,
  DHService,
  CommitteeService,
  NotiService,
  UserService,
  TrainingStaffService
};
