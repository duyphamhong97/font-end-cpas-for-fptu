import { baseServerAxios } from '../services/axios-provider';
import CommonUtil from '../common/utils/common-util';
import UrlConstant from '../common/constant/common-url';

export default {
  // Send topic to committee
  sendTopicToCommittee(data, callback, errorCallback) {
    CommonUtil.requestAxios(
      baseServerAxios().post(
        UrlConstant.api.dh.SEND_TOPIC_TO_COMMITTEE,
        JSON.stringify(data)
      ),
      callback,
      errorCallback
    );
  },
  // Send topic to trainning staff
  sendTopicToTrainningStaff(data, callback, errorCallback) {
    CommonUtil.requestAxios(
      baseServerAxios().post(
        UrlConstant.api.dh.SEND_TOPIC_TO_TRAINING_STAFF,
        JSON.stringify(data)
      ),
      callback,
      errorCallback
    );
  },
  // Send topic to storage
  sendTopicToStorage(data, callback, errorCallback) {
    CommonUtil.requestAxios(
      baseServerAxios().post(
        UrlConstant.api.dh.SEND_TOPIC_TO_STORAGE,
        JSON.stringify(data)
      ),
      callback,
      errorCallback
    );
  },
  // Department head het topic peding
  getPendingTopic(callback, errorCallback) {
    CommonUtil.requestAxios(
      baseServerAxios().get(UrlConstant.api.dh.GET_TOPIC_PENDING),
      callback,
      errorCallback
    );
  },
  // Department head get approved topic
  getApprovedTopic(callback, errorCallback) {
    CommonUtil.requestAxios(
      baseServerAxios().get(UrlConstant.api.dh.GET_DUPLICATE_APPROVED_TOPIC),
      callback,
      errorCallback
    );
  },
  // Department head get validation approved topic
  getValidationApprovedTopic(callback, errorCallback) {
    CommonUtil.requestAxios(
      baseServerAxios().get(UrlConstant.api.dh.GET_VALIDATION_APPROVED_TOPIC),
      callback,
      errorCallback
    );
  },
  // Get all question comment by topic id
  getAllQuestionCommentByTopicID(id, callback, errorCallback) {
    CommonUtil.requestAxios(
      baseServerAxios().get(
        UrlConstant.api.dh.GET_ALL_QUESTION_COMMENT_BY_TOPIC_ID.replace(
          '{id}',
          id
        )
      ),
      callback,
      errorCallback
    );
  },
  // Department head get topic review by Id
  getTopicReviewById(id, callback, errorCallback) {
    CommonUtil.requestAxios(
      baseServerAxios().get(
        UrlConstant.api.dh.GET_TOPIC_REVIEW_BY_ID.replace('{id}', id)
      ),
      callback,
      errorCallback
    );
  },
  // Post topic review
  postTopicReview(data, callback, errorCallback) {
    CommonUtil.requestAxios(
      baseServerAxios().post(
        UrlConstant.api.dh.POST_TOPIC_REVIEW_STATUS,
        JSON.stringify(data)
      ),
      callback,
      errorCallback
    );
  },
  // get group student
  getClassifyGroup(callback, errorCallback) {
    CommonUtil.requestAxios(
      baseServerAxios().get(UrlConstant.api.dh.GET_CLASSIFY_GROUP),
      callback,
      errorCallback
    );
  },
  // disable topic
  putDisableTopic(id, callback, errorCallback) {
    CommonUtil.requestAxios(
      baseServerAxios().put(
        UrlConstant.api.dh.PUT_DISABLE_TOPIC.replace('{id}', id)
      ),
      callback,
      errorCallback
    );
  },
  // Re-send topic to committee
  putTopicToCommittee(id, callback, errorCallback) {
    CommonUtil.requestAxios(
      baseServerAxios().put(
        UrlConstant.api.dh.PUT_TOPIC_TO_COMMITTEE.replace('{id}', id)
      ),
      callback,
      errorCallback
    );
  },
  // Send rewrite topic for advisor
  putRewriteTopic(id, callback, errorCallback) {
    CommonUtil.requestAxios(
      baseServerAxios().put(
        UrlConstant.api.dh.PUT_REWRITE_TOPIC.replace('{id}', id)
      ),
      callback,
      errorCallback
    );
  },
  // Get schedule
  getSchedule(callback, errorCallback) {
    CommonUtil.requestAxios(
      baseServerAxios().get(UrlConstant.api.dh.GET_SCHEDULE),
      callback,
      errorCallback
    );
  },
  // Update schedule expired time
  putSchedule(data, callback, errorCallback) {
    CommonUtil.requestAxios(
      baseServerAxios().put(
        UrlConstant.api.dh.PUT_SCHEDULE,
        JSON.stringify(data)
      ),
      callback,
      errorCallback
    );
  },
  // Get student weight
  getStudentWeight(callback, errorCallback) {
    CommonUtil.requestAxios(
      baseServerAxios().get(UrlConstant.api.dh.GET_STUDENT_WEIGHT),
      callback,
      errorCallback
    );
  },
  // Get student weight
  getStudentOfDepartmentHead(programId, callback, errorCallback) {
    CommonUtil.requestAxios(
      baseServerAxios().get(
        UrlConstant.api.dh.GET_NEW_STUDENT.replace('{programId}', programId)
      ),
      callback,
      errorCallback
    );
  },
  // Post student weight
  postStudentWeight(data, callback, errorCallback) {
    CommonUtil.requestAxios(
      baseServerAxios().put(
        UrlConstant.api.dh.PUT_STUDENT_WEIGHT,
        JSON.stringify(data)
      ),
      callback,
      errorCallback
    );
  },
  // Department head select student and advisor
  postStudentAdvisor(data, callback, errorCallback) {
    CommonUtil.requestAxios(
      baseServerAxios().post(
        UrlConstant.api.dh.POST_ADVISOR_STUDENT,
        JSON.stringify(data)
      ),
      callback,
      errorCallback
    );
  },
  // Department head post all student weight => List will be change status and remove from current table
  postAllStudentWeight(data, callback, errorCallback) {
    CommonUtil.requestAxios(
      baseServerAxios().post(
        UrlConstant.api.dh.POST_ADVISOR_STUDENT_ALL,
        JSON.stringify(data)
      ),
      callback,
      errorCallback
    );
  },
  // Get student grouping by id
  getStudentGroupingByProgramId(id, callback, errorCallback) {
    CommonUtil.requestAxios(
      baseServerAxios().get(
        UrlConstant.api.dh.GET_STUDENT_GROUPING_BY_ID.replace('{id}', id)
      ),
      callback,
      errorCallback
    );
  },
  // Post student grouping
  postStudentGrouping(data, callback, errorCallback) {
    CommonUtil.requestAxios(
      baseServerAxios().post(
        UrlConstant.api.dh.POST_STUDENT_GROUPING,
        JSON.stringify(data)
      ),
      callback,
      errorCallback
    );
  },
  // Post grouping student to trainning staff
  postStudentGroupingToTS(data, callback, errorCallback) {
    CommonUtil.requestAxios(
      baseServerAxios().post(
        UrlConstant.api.dh.POST_STUDENT_GROUPING_TO_TS,
        JSON.stringify(data)
      ),
      callback,
      errorCallback
    );
  },
  // delete student
  deleteStudent(code, callback, errorCallback) {
    CommonUtil.requestAxios(
      baseServerAxios().delete(
        UrlConstant.api.dh.DELETE_STUDENT.replace('{code}', code)
      ),
      callback,
      errorCallback
    );
  },
  // delete group
  deleteGroup(id, callback, errorCallback) {
    CommonUtil.requestAxios(
      baseServerAxios().delete(
        UrlConstant.api.dh.DELETE_GROUP.replace('{id}', id)
      ),
      callback,
      errorCallback
    );
  },
  // get group
  getGroup(id, callback, errorCallback) {
    CommonUtil.requestAxios(
      baseServerAxios().get(
        UrlConstant.api.dh.GET_GROUP.replace('{programId}', id)
      ),
      callback,
      errorCallback
    );
  },
  // get question
  getQuestion(callback, errorCallback) {
    CommonUtil.requestAxios(
      baseServerAxios().get(UrlConstant.api.dh.GET_QUESTION),
      callback,
      errorCallback
    );
  },
  // post comment
  postComment(data, callback, errorCallback) {
    CommonUtil.requestAxios(
      baseServerAxios().post(
        UrlConstant.api.dh.POST_COMMENT,
        JSON.stringify(data)
      ),
      callback,
      errorCallback
    );
  }
};
