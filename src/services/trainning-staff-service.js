import { baseServerAxios } from '../services/axios-provider';
import CommonUtil from '../common/utils/common-util';
import UrlConstant from '../common/constant/common-url';

export default {
  getGroupStudentBySemesterAndProgram(data, callback, errorCallback) {
    CommonUtil.requestAxios(
      baseServerAxios().get(
        UrlConstant.api.trainningStaff.GET_GROUP.replace(
          '{semesterId}',
          data.semesterId
        ).replace('{programId}', data.programId)
      ),
      callback,
      errorCallback
    );
  },
  // Download all topics and zip
  getDownloadAllTopicsAndZipUrl(topics) {
    const host = UrlConstant.api.ROOT;
    return `${host}${UrlConstant.api.trainningStaff.EXPORT_DOWLOAD_ALL}?topics=${topics}`;
  },
  getExportExcelListGroupStudentAfterPickTopic(data) {
    const host = UrlConstant.api.ROOT;
    return `${host}${UrlConstant.api.trainningStaff.EXPORT_EXCEL_GROUP_STUDENT_AFTER_PICK_TOPIC.replace(
      '{semesterId}',
      data.semesterId
    ).replace('{programId}', data.programId)}`;
  },
  getExportExcelListGroupStudentAfterImport(data) {
    const host = UrlConstant.api.ROOT;
    return `${host}${UrlConstant.api.trainningStaff.EXPORT_EXCEL_GROUP_STUDENT_AFTER_IMPORT.replace(
      '{semesterId}',
      data.semesterId
    ).replace('{programId}', data.programId)}`;
  },
  getAnnouncement(callback, errorCallback) {
    CommonUtil.requestAxios(
      baseServerAxios().get(UrlConstant.api.trainningStaff.GET_ANNOUNCEMENT),
      callback,
      errorCallback
    );
  },
  // import student
  updateAnnouncement(data, callback, errorCallback) {
    CommonUtil.requestAxios(
      baseServerAxios().post(
        UrlConstant.api.trainningStaff.POST_ANNOUNCEMENT,
        JSON.stringify(data)
      ),
      callback,
      errorCallback
    );
  }
};
