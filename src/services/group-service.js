import { baseServerAxios } from '../services/axios-provider';
import CommonUtil from '../common/utils/common-util';
import UrlConstant from '../common/constant/common-url';

export default {
  // classify group student
  getGroup(callback, errorCallback) {
    CommonUtil.requestAxios(
      baseServerAxios().get(UrlConstant.api.group.GET_GROUP_STUDENT),
      callback,
      errorCallback
    );
  },
  // get group student
  getGroupStudent(callback, errorCallback) {
    CommonUtil.requestAxios(
      baseServerAxios().get(UrlConstant.api.group.TS_GET_GROUP_STUDENT),
      callback,
      errorCallback
    );
  },
  // set duration
  makeSchedule(data, callback, errorCallback) {
    CommonUtil.requestAxios(
      baseServerAxios().post(
        UrlConstant.api.trainningStaff.MAKE_SCHEDULE_GROUP_STUDENT,
        data
      ),
      callback,
      errorCallback
    );
  },
  // submit schedule
  submitSchedule(callback, errorCallback) {
    CommonUtil.requestAxios(
      baseServerAxios().post(UrlConstant.api.trainningStaff.SUBMIT_SCHEDULE),
      callback,
      errorCallback
    );
  }
};
