import { baseServerAxios } from '../services/axios-provider';
import CommonUtil from '../common/utils/common-util';
import UrlConstant from '../common/constant/common-url';

export default {
  // Get advisor
  getAdvisor(callback, errorCallback) {
    CommonUtil.requestAxios(
      baseServerAxios().get(UrlConstant.api.advisor.GET_ADVISOR),
      callback,
      errorCallback
    );
  },
  getStudentPoint(callback, errorCallback) {
    CommonUtil.requestAxios(
      baseServerAxios().get(UrlConstant.api.advisor.GET_STUDENT_POINT),
      callback,
      errorCallback
    );
  },
  postStudentPoint(data, callback, errorCallback) {
    CommonUtil.requestAxios(
      baseServerAxios().post(UrlConstant.api.advisor.POST_STUDENT_POINT, data),
      callback,
      errorCallback
    );
  },
  getStudentByTopicId(id, callback, errorCallback) {
    CommonUtil.requestAxios(
      baseServerAxios().get(
        UrlConstant.api.advisor.GET_STUDENT_BY_TOPIC_ID.replace('{id}', id)
      ),
      callback,
      errorCallback
    );
  },
  getStudentGradeByCode(id, callback, errorCallback) {
    CommonUtil.requestAxios(
      baseServerAxios().get(
        UrlConstant.api.advisor.GET_STUDENT_GRADE_BY_CODE.replace('{code}', id)
      ),
      callback,
      errorCallback
    );
  },
  postGradeStudentByGradeId(data, callback, errorCallback) {
    CommonUtil.requestAxios(
      baseServerAxios().post(
        UrlConstant.api.advisor.POST_STUDENT_GRADE_BY_ID,
        data
      ),
      callback,
      errorCallback
    );
  },
  postGradeStudent(data, callback, errorCallback) {
    CommonUtil.requestAxios(
      baseServerAxios().post(UrlConstant.api.advisor.POST_GRADE_STUDENT, data),
      callback,
      errorCallback
    );
  }
};
