import { baseServerAxios } from '../services/axios-provider';
import CommonUtil from '../common/utils/common-util';
import UrlConstant from '../common/constant/common-url';

export default {
  // import student
  createStudent(data, callback, errorCallback) {
    CommonUtil.requestAxios(
      baseServerAxios().post(
        UrlConstant.api.trainningStaff.CREATE_STUDENT,
        JSON.stringify(data)
      ),
      callback,
      errorCallback
    );
  },
  // student pick topic
  pickTopic(data, callback, errorCallback) {
    CommonUtil.requestAxios(
      baseServerAxios().put(UrlConstant.api.group.STUDENT_PICK_TOPIC, data),
      callback,
      errorCallback
    );
  },
  // get students
  getStudent(isNew, callback, errorCallback) {
    CommonUtil.requestAxios(
      baseServerAxios().get(UrlConstant.api.trainningStaff.GET_STUDENT, {
        params: {
          isNew: isNew
        }
      }),
      callback,
      errorCallback
    );
  },
  // get announcement
  getAnnouncement(callback, errorCallback) {
    CommonUtil.requestAxios(
      baseServerAxios().get(UrlConstant.api.student.GET_ANNOUNCEMENT),
      callback,
      errorCallback
    );
  },
  // get unauthorize announcement
  getUnauthorizeAnnouncement(callback, errorCallback) {
    CommonUtil.requestAxios(
      baseServerAxios().get(UrlConstant.api.student.GET_ANNOUNCEMENT),
      callback,
      errorCallback
    );
  },
  // ts send list student to dh
  sendStudentToDh(data, callback, errorCallback) {
    CommonUtil.requestAxios(
      baseServerAxios().put(
        UrlConstant.api.trainningStaff.PUT_STUDENT_DH,
        JSON.stringify(data)
      ),
      callback,
      errorCallback
    );
  },
  // ts get pick time
  getPickTime(callback, errorCallback) {
    CommonUtil.requestAxios(
      baseServerAxios().get(UrlConstant.api.student.GET_PICK_TIME),
      callback,
      errorCallback
    );
  },
  // st get students in group
  getStudentsInGroup(callback, errorCallback) {
    CommonUtil.requestAxios(
      baseServerAxios().get(UrlConstant.api.student.GET_STUDENT_IN_GROUP),
      callback,
      errorCallback
    );
  },
  getStudentGrade(callback, errorCallback) {
    CommonUtil.requestAxios(
      baseServerAxios().get(UrlConstant.api.student.GET_STUDENT_GRADE),
      callback,
      errorCallback
    );
  }
};
