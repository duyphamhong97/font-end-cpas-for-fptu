export default {
  CONFIG: 'config',
  STATUS: {
    DRAFT: 'Draft',
    PENDING: 'Pending',
    PUBLIC: 'Published',
    UNPUBLISH: 'Unpublished'
  },
  STATUS_TO_ID: {
    DRAFT: 1,
    PENDING: 2,
    VALIDATEAPPROVED: 6,
    UNPUBLISH: 7,
    PUBLIC: 8
  },
  NOTI_TYPE: {
    SUCCESS: 'success',
    WARN: 'warn',
    ERROR: 'error',
    INFO: 'info'
  },
  DEFAULT_TIME_FORMAT: 'DD/MM/YYYY HH:mm:ss',
  DEFAULT_TIME_RANGE_FORMAT: 'DD/MM/YYYY HH:mm',
  DEFAULT_DATE_FORMAT: 'DD/MM/YYYY',
  DEFAULT_TIME_COUNTING_FORMAT: 'HH:mm:ss',
  ROLE: {
    '0': 'student',
    '1': 'trainning staff',
    '2': 'advisor',
    '3': 'committee',
    '4': 'department head'
  },
  REGEX: {
    standard: /^[\w !#$%&'*+/=?^_`{|}~-]+$/,
    email: /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[a-z]{2}|com|org|net|gov|mil|biz|info|mobi|name|aero|jobs|museum)+$/
  },
  DEFAULT_EXPIRED_TIME_COOKIE: 2 * 3600000, // 60 minutes,
  DEFAULT_CHECK_EXPIRE_TOKEN_INTERVAL: 60000, // 1 minute
  DEFAULT_DANGER_DURATION: 600000 // 10 minutes
};
