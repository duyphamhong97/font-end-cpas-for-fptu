export default {
  page: {
    DEFAULT: '/login',
    error: {
      ERROR: '/error/:id'
    },
    user: {
      LOGIN: '/login'
    },
    advisor: {
      TOPIC_MNG: '/as/topic-mng',
      CREATE_TOPIC: '/as/create-topic/:id',
      SEARCH_TOPIC: '/as/search-topic',
      TOPIC_DETAIL: '/as/topic-detail/:id',
      QUESTION_TABLE: '/as/question-table/:id',
      TOPIC_DUPLICATE_DETAIL: '/as/topic-duplicate-detail/:id',
      GET_TOPIC: '/as/get-topic',
      EVALUATE_STUDENT: '/as/evaluate-student',
      STUDENTS_IN_TOPIC: '/as/students-in-topic/:id'
    },
    committe: {
      CM_GETLIST_TOPIC: '/ct/get-topic',
      CM_TOPIC_DETAIL: '/ct/topic-detail/:id'
    },
    trainningStaff: {
      TS_PUBLISHED_TOPIC: '/ts/published-topic',
      TS_STUDENT_MNG: '/ts/student-mng',
      TS_IMPORT_STUDENT: '/ts/import-student',
      TS_GROUP_TOPIC: '/ts/group-topic',
      TS_GROUP_STUDENT_SCHEDULE: '/ts/create-schedule-group',
      TS_SCHEDULE_GROUP: '/ts/schedule-group',
      TS_ANNOUNCEMENT: '/ts/announcement',
      TOPIC_DETAIL: '/ts/topic-detail/:id'
    },
    student: {
      ST_LIST_TOPIC: '/student/list-topic',
      ST_TOPIC_DETAIL: '/student/topic-detail/:id',
      ST_TOPIC_IN_PROGRESS: '/student/student-progress',
      ST_LIST_STUDENT_IN_GROUP: '/student/student-group',
      ST_DASHBOARD: '/student/dashboard'
    },
    departmentHead: {
      DH_TECHNIQUE_MNG: '/dh/technique-mng',
      DH_GET_STUDENT: '/dh/student-list',
      DH_SEND_TOPIC: '/dh/send-topic',
      DH_MNG_TOPIC: '/dh/manage-topic',
      DH_VALIDATE_TOPIC: '/dh/validate-topic',
      DH_STORE_TOPIC: '/dh/store-topic',
      DH_QUESTION_TABLE: '/dh/question-table/:id',
      DH_TOPIC_DETAIL: '/dh/topic-detail/:id',
      DH_SCHEDULE: '/dh/schedule',
      DH_WEIGHT_MNG: '/dh/weight-mng',
      DH_AD_STU_MNG: '/dh/advisor-student-mng',
      DH_CLASS_STU: '/dh/classified-student',
      DH_CLASS_STU_V2: '/dh/classified-student-v2',
      DH_DELETE_GROUPSTUDENT: '/dh/delete-groupstudent',
      DH_APPROVED_TOPIC: '/dh/approved-topic'
    }
  },
  api: {
    ROOT: 'https://cpaf-api.azurewebsites.net/api/',
    // ROOT: 'http://localhost:60192/api/',
    common: {
      GET_CONFIG: '/config'
    },
    user: {
      LOGIN: '/auth/login'
    },
    technique: {
      GET_TECHNIQUE: '/technique',
      POST_TECHNIQUE: '/technique'
    },
    program: {
      GET_PROGRAM: '/program'
    },
    question: {
      GET_QUESTION: '/committee/question',
      POST_COMMENT: '/committee/question',
      GET_COMMITTE_COMMENT: '/comment/{topicId}'
    },
    semester: {
      GET_SEMESTER: '/semester'
    },
    advisor: {
      CREATE_TOPIC: '/topic',
      UPDATE_TOPIC: '/topic',
      GET_ADVISOR: '/advisor',
      GET_STUDENT_POINT: '/advisor/student-point',
      POST_STUDENT_POINT: '/advisor/student-point',
      GET_STUDENT_BY_TOPIC_ID: '/advisor/topic/{id}/student',
      GET_STUDENT_GRADE_BY_CODE: '/advisor/student/{code}/grade',
      POST_STUDENT_GRADE_BY_ID: '/advisor/student/grade',
      POST_GRADE_STUDENT: '/advisor/topic/student'
    },
    topic: {
      ADVISOR_GET_TOPIC: '/advisor/topic',
      GET_TOPIC_BY_ID: '/topic/{id}',
      SEARCH_TOPIC_WITH_PAGINATION:
        '/topics/search?name={name}&pageIndex={pageIndex}&pageSize={pageSize}&isAsc={isAsc}',
      GET_TOPIC_BY_STATUS: '/ts/topic',
      POST_TOPIC_STATUS: '/topic/status',
      GET_DH_TOPIC_REVIEW: '/dh/topic/review',
      TS_POST_TOPIC_STATUS: '/ts/topic/status',
      STUDENT_GET_TOPIC: '/student/topic'
    },
    dh: {
      SEND_TOPIC_TO_COMMITTEE: '/dh/send-to-committee',
      SEND_TOPIC_TO_TRAINING_STAFF: '/dh/topic/send-to-training-staff',
      SEND_TOPIC_TO_STORAGE: '/dh/topic/send-to-storage',
      GET_DUPLICATE_APPROVED_TOPIC: '/dh/topic/status/duplicate-approved',
      GET_VALIDATION_APPROVED_TOPIC: '/dh/topic/status/validation-approved',
      GET_ALL_QUESTION_COMMENT_BY_TOPIC_ID: '/dh/topic/{id}/comment',
      GET_TOPIC_REVIEW_BY_ID: '/dh/topic/{id}/review',
      POST_TOPIC_REVIEW_STATUS: '/dh/topic/review',
      GET_CLASSIFY_GROUP: 'dh/classify-group',
      PUT_DISABLE_TOPIC: 'dh/topic/{id}/disable',
      PUT_REWRITE_TOPIC: 'dh/topic/{id}/rewrite',
      PUT_TOPIC_TO_COMMITTEE: 'dh/topic/{id}/to-committee',
      GET_SCHEDULE: '/dh/comment/schedule',
      PUT_SCHEDULE: '/dh/comment/schedule',
      GET_STUDENT_WEIGHT: '/dh/student/weight',
      PUT_STUDENT_WEIGHT: '/dh/student/weight',
      POST_ADVISOR_STUDENT: '/dh/send-to-advisor',
      POST_ADVISOR_STUDENT_ALL: '/dh/student/weight/all',
      GET_NEW_STUDENT: '/dh/student/new/{programId}',
      GET_STUDENT_GROUPING_BY_ID: '/dh/student/grouping/{id}',
      POST_STUDENT_GROUPING: '/dh/student/grouping',
      POST_STUDENT_GROUPING_TO_TS: '/dh/student/grouping/send-ts',
      DELETE_STUDENT: '/dh/student/{code}',
      DELETE_GROUP: '/dh/group/{id}',
      GET_GROUP: '/dh/student/grouping/{programId}',
      GET_TOPIC_PENDING: '/dh/topic/status/publish-pending',
      GET_QUESTION: '/committee/question',
      POST_COMMENT: '/dh/topic/review'
    },
    group: {
      GET_GROUP_STUDENT: '/group',
      STUDENT_PICK_TOPIC: '/student/topic',
      TS_GET_GROUP_STUDENT: '/ts/schedule/groupStudent'
    },
    trainningStaff: {
      CREATE_STUDENT: '/student',
      GET_STUDENT: '/ts/student',
      PUT_STUDENT_DH: '/ts/student/send-dh',
      MAKE_SCHEDULE_GROUP_STUDENT: '/ts/schedule/groupStudent',
      SUBMIT_SCHEDULE: '/ts/groupStudent/start-schedule',
      GET_GROUP: '/ts/groupStudent/topic/{semesterId}/{programId}',
      DOWLOAD_ALL: '/ts/download-all',
      EXPORT_DOWLOAD_ALL: '/ts/export-word',
      EXPORT_EXCEL_GROUP_STUDENT_AFTER_PICK_TOPIC:
        'ts/export-excel/{semesterId}/{programId}',
      EXPORT_EXCEL_GROUP_STUDENT_AFTER_IMPORT:
        'ts/groupStudent/export-excel/{semesterId}/{programId}',
      GET_ANNOUNCEMENT: '/ts/announcement',
      POST_ANNOUNCEMENT: '/ts/announcement'
    },
    student: {
      GET_PICK_TIME: '/student/topic/pick-time',
      GET_STUDENT_IN_GROUP: '/student/group',
      GET_STUDENT_GRADE: '/student/grade',
      GET_ANNOUNCEMENT: '/student/announcement',
      GET_UNAUTHORIZE_ANNOUNCEMENT: '/unauthorize/announcement'
    },
    committee: {
      GET_TOPIC: '/committee/topic/'
    },
    notification: {
      CREATE_NOTI: '/notification'
    }
  }
};
