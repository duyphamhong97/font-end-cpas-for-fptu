export default {
  // Handle error
  handleError(errorObj, nameInputText, inputData, isRequired, min, max, regex) {
    const errorCode = this.validateField(
      inputData,
      isRequired,
      min,
      max,
      regex
    );
    if (!errorObj.isError && errorCode > 0) {
      errorObj.isError = true;
    } else {
      errorObj.isError = false;
    }
    return this.getTextError(errorCode, nameInputText, min, max);
  },
  // Validate field input
  // 0: Valid
  // 1: Empty input
  // 2: Min is not valid
  // 3: Max is not valid
  // 4: Invalid
  validateField(inputData, isRequired, min, max, regex) {
    // Case empty
    if (isRequired && (!inputData || inputData.length === 0)) return 1;
    // Case min length
    if (min && min !== 0 && inputData.length < min) return 2;
    // Case max length
    if (max && max !== 0 && inputData.length > max) return 3;
    // Case text invalid
    if (regex && !regex.test(inputData)) return 4;
    return 0;
  },
  // Get text error
  getTextError(errorCode, nameInputText, min, max) {
    let name = 'This field';
    let errorText = '';
    if (nameInputText) {
      name = nameInputText;
    }
    switch (errorCode) {
      case 1:
        errorText = '{name} cannot be empty!';
        break;
      case 2:
        errorText = '{name} must be at least {min}!';
        errorText = errorText.replace('{min}', min);
        break;
      case 3:
        errorText = '{name} must be at max {max}!';
        errorText = errorText.replace('{max}', max);
        break;
      case 4:
        errorText = '{name} is invalid!';
        break;
    }
    errorText = errorText.replace('{name}', name);
    return errorText;
  }
};
