import store from '../store';
import CommonUtil from '../common/utils/common-util';

export default {
  // Check cookies
  isLoginExpired(accessToken) {
    let isExpired = true;
    // If access token is not expired
    if (accessToken) {
      isExpired = false;
    }
    return isExpired;
  },
  initUserLogin(accessToken) {
    let isValid = false;
    const info = localStorage.getItem('userInfo');
    if (accessToken && info) {
      isValid = true;
    } else {
      isValid = false;
    }
    store.dispatch('user/setToken', accessToken);
    store.dispatch('user/login', JSON.parse(info));
    return isValid;
  },
  cleanUserData() {
    CommonUtil.removeCookies('access_token');
    localStorage.removeItem('userInfo');
    store.dispatch('user/setToken', '');
    store.dispatch('user/login', '');
  }
};
