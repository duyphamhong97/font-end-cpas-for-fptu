import Vue from 'vue';
import Router from 'vue-router';
import UrlConstant from '../common/constant/common-url';
import CommonUtil from '../common/utils/common-util';
import RouterGuard from './router-guard';
// Error
import ErrorPage from '../../src/pages/error/ErrorPage.vue';
// Advisor
import AdvisorTopicMng from '../pages/advisor/TopicMng.vue';
import AdvisorCreateTopic from '../pages/advisor/CreateTopic.vue';
import AdvisorSearchTopic from '../pages/advisor/SearchTopic.vue';
import AdvisorTopicDetail from '../pages/advisor/TopicDetail.vue';
import AdvisorTopicDuplicateDetail from '../pages/advisor/TopicDuplicateReview.vue';
import AdvisorQuestionTable from '../pages/advisor/components/QuestionTable.vue';
import EvaluateStudent from '../pages/advisor/EvaluateStudent.vue';
import StudentsInTopic from '../pages/advisor/StudentsInTopic.vue';
// Training Staff
import TrainningStaffPublishedTopic from '../pages/trainningStaff/ListingCapstone.vue';
import TrainningStaffStudentMng from '../pages/trainningStaff/StudentMng.vue';
import TrainingStaffImportStudent from '../pages/trainningStaff/ImportStudent.vue';
import TrainningStaffGroupTopic from '../pages/trainningStaff/GroupTopic.vue';
import TrainningStaffScheduleGroup from '../pages/trainningStaff/ScheduleGroup.vue';
import TrainningStaffAnnouncement from '../pages/trainningStaff/Announcement.vue';
import TrainningStaffTopicDetail from '../pages/trainningStaff/TopicDetail.vue';

// Student
import StudentListTopic from '../pages/student/ListTopic.vue';
import StudentGetTopicDetail from '../pages/student/StudentGetTopicDetail.vue';
import StudentTopicInProgress from '../pages/student/StudentTopicInProgress.vue';
import ListStudentInGroup from '../pages/student/ListStudentInGroup.vue';
import StudentDashboard from '../pages/student/Dashboard.vue';
// department head
import DepartmentHeadGetListStudent from '../pages/departmentHead/GetListStudent.vue';
import DepartmentHeadSendTopic from '../pages/departmentHead/SendTopic.vue';
import DepartmentHeadManageTopic from '../pages/departmentHead/ManageTopic.vue';
import DepartmentHeadQuestionTable from '../pages/departmentHead/components/approve-topic/QuestionTable.vue';
import DepartmentHeadTopicDetail from '../pages/departmentHead/DhTopicDetail.vue';
import DepartmentHeadSchedule from '../pages/departmentHead/Schedule.vue';
import DepartmentHeadStudentWeight from '../pages/departmentHead/WeightMng.vue';
import DepartmentHeadAdvisorStudent from '../pages/departmentHead/AdvisorStudentMng.vue';
import DepartmentHeadClassifiedStudent from '../pages/departmentHead/ClassifiedStudent.vue';
import DepartmentHeadClassifiedStudentV2 from '../pages/departmentHead/ClassifiedStudentV2.vue';
import DepartmentHeadDeleteGroupStudent from '../pages/departmentHead/DeleteGroupStudent.vue';
import DepartmentHeadValidateTopic from '../pages/departmentHead/ValidateTopic.vue';
import DepartmentHeadStoreTopic from '../pages/departmentHead/StorageTopic.vue';
import DepartmentHeadApprovedTopic from '../pages/departmentHead/ApprovedTopic.vue';
import DepartmentHeadTechnique from '../pages/departmentHead/Technique.vue';

// committee
import CommitteeGetTopic from '../pages/committee/GetTopic.vue';
import CommitteGetTopicDetail from '../pages/committee/TopicDetail.vue';
// user
import Login from '../pages/user/Login.vue';

Vue.use(Router);

const router = new Router({
  routes: [
    {
      path: UrlConstant.page.DEFAULT,
      name: 'Default',
      component: Login
    },
    {
      path: UrlConstant.page.error.ERROR,
      name: 'Error',
      component: ErrorPage
    },
    {
      path: UrlConstant.page.trainningStaff.TS_PUBLISHED_TOPIC,
      name: 'TsPublishedTopics',
      component: TrainningStaffPublishedTopic
    },
    {
      path: UrlConstant.page.trainningStaff.TS_IMPORT_STUDENT,
      name: 'TsImportStudent',
      component: TrainingStaffImportStudent
    },
    {
      path: UrlConstant.page.trainningStaff.TS_STUDENT_MNG,
      name: 'TsStudentMng',
      component: TrainningStaffStudentMng
    },
    {
      path: UrlConstant.page.trainningStaff.TS_SCHEDULE_GROUP,
      name: 'TsScheduleGroup',
      component: TrainningStaffScheduleGroup
    },
    {
      path: UrlConstant.page.trainningStaff.TS_GROUP_TOPIC,
      name: 'TsGroupTopic',
      component: TrainningStaffGroupTopic
    },
    {
      path: UrlConstant.page.trainningStaff.TS_ANNOUNCEMENT,
      name: 'TsAnnouncement',
      component: TrainningStaffAnnouncement
    },
    {
      path: UrlConstant.page.trainningStaff.TOPIC_DETAIL,
      name: 'TsTopicDetail',
      component: TrainningStaffTopicDetail
    },
    {
      path: UrlConstant.page.departmentHead.DH_DELETE_GROUPSTUDENT,
      name: 'DhDeleteGroupStudent',
      component: DepartmentHeadDeleteGroupStudent
    },
    {
      path: UrlConstant.page.departmentHead.DH_GET_STUDENT,
      name: 'DhGetStudent',
      component: DepartmentHeadGetListStudent
    },
    {
      path: UrlConstant.page.departmentHead.DH_QUESTION_TABLE,
      name: 'DhQuestionTable',
      component: DepartmentHeadQuestionTable
    },
    {
      path: UrlConstant.page.departmentHead.DH_SEND_TOPIC,
      name: 'DhSendTopic',
      component: DepartmentHeadSendTopic
    },
    {
      path: UrlConstant.page.departmentHead.DH_APPROVED_TOPIC,
      name: 'DhApprovedTopic',
      component: DepartmentHeadApprovedTopic
    },
    {
      path: UrlConstant.page.departmentHead.DH_WEIGHT_MNG,
      name: 'DhStudentWeight',
      component: DepartmentHeadStudentWeight
    },
    {
      path: UrlConstant.page.departmentHead.DH_MNG_TOPIC,
      name: 'DhManageTopic',
      component: DepartmentHeadManageTopic
    },
    {
      path: UrlConstant.page.departmentHead.DH_SCHEDULE,
      name: 'DhSchedule',
      component: DepartmentHeadSchedule
    },
    {
      path: UrlConstant.page.departmentHead.DH_AD_STU_MNG,
      name: 'DhAdvisorStudent',
      component: DepartmentHeadAdvisorStudent
    },
    {
      path: UrlConstant.page.departmentHead.DH_CLASS_STU,
      name: 'DhClassifiedStudent',
      component: DepartmentHeadClassifiedStudentV2
    },
    {
      path: UrlConstant.page.departmentHead.DH_CLASS_STU_V2,
      name: 'DhClassifiedStudentV2',
      component: DepartmentHeadClassifiedStudent
    },
    {
      path: UrlConstant.page.departmentHead.DH_VALIDATE_TOPIC,
      name: 'DhValidateTopic',
      component: DepartmentHeadValidateTopic
    },
    {
      path: UrlConstant.page.departmentHead.DH_STORE_TOPIC,
      name: 'DhStoreTopic',
      component: DepartmentHeadStoreTopic
    },
    {
      path: UrlConstant.page.departmentHead.DH_TECHNIQUE_MNG,
      name: 'DhTechniqueMng',
      component: DepartmentHeadTechnique
    },
    {
      path: UrlConstant.page.departmentHead.DH_TOPIC_DETAIL,
      name: 'Detail topic',
      component: DepartmentHeadTopicDetail
    },
    {
      path: UrlConstant.page.student.ST_LIST_TOPIC,
      name: 'StListTopic',
      component: StudentListTopic
    },
    {
      path: UrlConstant.page.student.ST_TOPIC_DETAIL,
      name: 'StGetTopicDetail',
      component: StudentGetTopicDetail
    },
    {
      path: UrlConstant.page.student.ST_TOPIC_IN_PROGRESS,
      name: 'StTopicInProgress',
      component: StudentTopicInProgress
    },
    {
      path: UrlConstant.page.student.ST_DASHBOARD,
      name: 'StDashboard',
      component: StudentDashboard
    },
    {
      path: UrlConstant.page.student.ST_LIST_STUDENT_IN_GROUP,
      name: 'StListStudentInGroup',
      component: ListStudentInGroup
    },
    {
      path: UrlConstant.page.committe.CM_GETLIST_TOPIC,
      name: 'CmGetListTopic',
      component: CommitteeGetTopic
    },
    {
      path: UrlConstant.page.committe.CM_TOPIC_DETAIL,
      name: 'CmTopicDetail',
      component: CommitteGetTopicDetail
    },
    {
      path: UrlConstant.page.advisor.TOPIC_MNG,
      name: 'AdvisorTopicMng',
      component: AdvisorTopicMng
    },
    {
      path: UrlConstant.page.advisor.CREATE_TOPIC,
      name: 'AdvisorCreateTopic',
      component: AdvisorCreateTopic
    },
    {
      path: UrlConstant.page.advisor.SEARCH_TOPIC,
      name: 'AdvisorSearchTopic',
      component: AdvisorSearchTopic
    },
    {
      path: UrlConstant.page.advisor.TOPIC_DETAIL,
      name: 'AdvisorTopicDetail',
      component: AdvisorTopicDetail
    },
    {
      path: UrlConstant.page.advisor.TOPIC_DUPLICATE_DETAIL,
      name: 'AdvisorTopicDuplicateDetail',
      component: AdvisorTopicDuplicateDetail
    },
    {
      path: UrlConstant.page.advisor.QUESTION_TABLE,
      name: 'AdvisorQuestionTable',
      component: AdvisorQuestionTable
    },
    {
      path: UrlConstant.page.advisor.EVALUATE_STUDENT,
      name: 'AdvisorEvaluateStudent',
      component: EvaluateStudent
    },
    {
      path: UrlConstant.page.advisor.STUDENTS_IN_TOPIC,
      name: 'AdvisorStudentsInTopic',
      component: StudentsInTopic
    },
    {
      path: UrlConstant.page.user.LOGIN,
      name: 'Log in',
      component: Login
    },
    { path: '*', redirect: UrlConstant.page.DEFAULT }
  ],
  mode: 'history'
});

router.beforeEach((to, from, next) => {
  const accessToken = CommonUtil.getCookies('access_token');
  if (
    to.path !== UrlConstant.page.user.LOGIN &&
    from.path !== UrlConstant.page.DEFAULT
  ) {
    const isLoginExpired = RouterGuard.isLoginExpired(accessToken);
    // Check expired
    if (isLoginExpired) {
      localStorage.removeItem('userInfo');
      const pathName = window.location.pathname;
      if (pathName !== UrlConstant.page.user.LOGIN) {
        next('/');
      }
    } else {
      handleValidLoginData(accessToken, to, next);
    }
  } else {
    handleValidLoginData(accessToken, to, next);
  }
});

const handleValidLoginData = (accessToken, to, next) => {
  const isValid = RouterGuard.initUserLogin(accessToken);
  // Handle local storage data
  if (isValid) {
    next();
  } else {
    RouterGuard.cleanUserData();
    if (to.path === UrlConstant.page.user.LOGIN) {
      next();
    } else {
      next('/');
    }
  }
};

export default router;
